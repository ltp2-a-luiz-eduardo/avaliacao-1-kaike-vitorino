package main

import (
	"net/http"
	"strconv"
)

// Struct dos parametros da url
type ParametrosURL struct{}

// ObterParametros pega os parametros e diz a acao e os numeros
// Retorna a operacao (string), o primeiro numero (float64), o segundo numero (float64) e um erro
func (p *ParametrosURL) ObterParametros(res http.ResponseWriter, req *http.Request) (string, int, int, error) {
	// Pega os parametros da url
	params := req.URL.Query()

	// Desconre qual a acao, o numero 1 e o numero 2
	action := params.Get("action")
	num1 := params.Get("num1")
	num2 := params.Get("num2")

	// Verifica se todos os parametros existem
	if action == "" || num1 == "" || num2 == "" {
		// Utiliza a funcao de resposta de operacao invalida em JSON
		//SendInvalidOperationResponse(res, "invalid expression")
		return "", 0, 0, nil
	}
	
	// Converte o numero 1 para float
	num1Float, err := strconv.ParseInt(num1, 10, 64)
	if err != nil {
		// Utiliza a funcao de resposta de operacao invalida em JSON
		//SendInvalidOperationResponse(res, "Numero 1 ta errado!")
		return "", 0, 0, nil
	}

	// Converte o numero 2 para float
	num2Float, err := strconv.ParseInt(num2, 10, 64)
	if err != nil {
		// Utiliza a funcao de resposta de operacao invalida em JSON
		//SendInvalidOperationResponse(res, "Numero 2 ta errado!")
		return "", 0, 0, nil
	}
	
	// Verifica se todos os parametros existem
	if num1Float <= 999 || num2Float <= 999 {
		
		return "", 0, 0, nil
	}
	
	// Retrornar a acao, os numeros convertidos e nenhum erro
	return action, int(num1Float), int(num2Float), nil
}
