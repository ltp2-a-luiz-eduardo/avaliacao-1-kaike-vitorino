package main

import (
	"math"
	"net/http"
	"fmt"
	"strconv"
	"strings"
)

// Struct para calculadora, ou seja, sua estrutura
type Calculator struct{}

// Aqui se define a interface para as operacoes da calculadora
type Operacoes interface {
	Operacao(a, b float64) string
}

// Struct para a operacao de adicao ou +
type Add struct{}

// Funcao onde ocorre a soma dos numeros
func (Add) Operacao(a, b float64) string {
	soma := a + b
	//somastr := strconv.FormatFloat(soma,'E', -1 , 64)
	//fmt.Println(somastr)
	
	resp := soma/1000
	fmt.Println(resp)
	respstr := strconv.FormatFloat(resp, 'E', -1, 64)

	respstr = strings.Replace(respstr, "E+00", "", 3)
	respstr = strings.Replace(respstr, "E+01", "", 3)
	respstr = strings.Replace(respstr, "E+02", "", 3)
	respstr = strings.Replace(respstr, "E+03", "", 3)
	respstr = strings.Replace(respstr, "E+04", "", 3) // Pode chorar, eu chorei tambem dpos de fazer essa bosta
	respstr = strings.Replace(respstr, "E+05", "", 3)
	respstr = strings.Replace(respstr, "E+06", "", 3)
	respstr = strings.Replace(respstr, "E+07", "", 3)
	respstr = strings.Replace(respstr, "E+08", "", 3)
	respstr = strings.Replace(respstr, "E+09", "", 3)

	resposta := (respstr + "×10^3")
	fmt.Println("Resposta:", resposta)
	return resposta
}

// Struct para a operacao de subtracao ou -
type Sub struct{}

// Funcao onde ocorre a subtracao dos numeros
func (Sub) Operacao(a, b float64) string {
	sub := a - b
	resp := sub/1000
	fmt.Println(resp)
	respstr := strconv.FormatFloat(resp, 'E', -1, 64)

	respstr = strings.Replace(respstr, "E+00", "", 3)
	respstr = strings.Replace(respstr, "E+01", "", 3)
	respstr = strings.Replace(respstr, "E+02", "", 3)
	respstr = strings.Replace(respstr, "E+03", "", 3)
	respstr = strings.Replace(respstr, "E+04", "", 3) // Pode chorar, eu chorei tambem dpos de fazer essa bosta
	respstr = strings.Replace(respstr, "E+05", "", 3)
	respstr = strings.Replace(respstr, "E+06", "", 3)
	respstr = strings.Replace(respstr, "E+07", "", 3)
	respstr = strings.Replace(respstr, "E+08", "", 3)
	respstr = strings.Replace(respstr, "E+09", "", 3)

	resposta := (respstr + "×10^3")
	fmt.Println("Resposta:", resposta)
	return resposta
}

// Struct para a operacao de multiplica ou *
type Mul struct{}

// Funcao onde ocorre a multiplicacao dos numeros
func (Mul) Operacao(a, b float64) string {
	mul1 := a * b
	resp := mul1/1000
	fmt.Println(resp)
	respstr := strconv.FormatFloat(resp, 'E', -1, 64)

	respstr = strings.Replace(respstr, "E+00", "", 3)
	respstr = strings.Replace(respstr, "E+01", "", 3)
	respstr = strings.Replace(respstr, "E+02", "", 3)
	respstr = strings.Replace(respstr, "E+03", "", 3)
	respstr = strings.Replace(respstr, "E+04", "", 3) // Pode chorar, eu chorei tambem dpos de fazer essa bosta
	respstr = strings.Replace(respstr, "E+05", "", 3)
	respstr = strings.Replace(respstr, "E+06", "", 3)
	respstr = strings.Replace(respstr, "E+07", "", 3)
	respstr = strings.Replace(respstr, "E+08", "", 3)
	respstr = strings.Replace(respstr, "E+09", "", 3)

	resposta := (respstr + "×10^3")
	fmt.Println("Resposta:", resposta)
	
	return resposta
}

// Struct para a operacao de divisao
type Div struct{}

// Funcao onde se te a operacao de dibisao dos numeros
func (Div) Operacao(a, b float64) string {
	if b == 0 {
		return "NAo se divide por 0"
	}
	Div := a/b
	resp := Div/1000
	fmt.Println(resp)
	respstr := strconv.FormatFloat(resp, 'E', -1, 64)

	respstr = strings.Replace(respstr, "E+00", "", 3)
	respstr = strings.Replace(respstr, "E+01", "", 3)
	respstr = strings.Replace(respstr, "E+02", "", 3)
	respstr = strings.Replace(respstr, "E+03", "", 3)
	respstr = strings.Replace(respstr, "E+04", "", 3) // Pode chorar, eu chorei tambem dpos de fazer essa bosta
	respstr = strings.Replace(respstr, "E+05", "", 3)
	respstr = strings.Replace(respstr, "E+06", "", 3)
	respstr = strings.Replace(respstr, "E+07", "", 3)
	respstr = strings.Replace(respstr, "E+08", "", 3)
	respstr = strings.Replace(respstr, "E+09", "", 3)

	resposta := (respstr + "×10^3")
	fmt.Println("Resposta:", resposta)
	return resposta
}

// Struct para a operacao (POW) de exponenciacao ou ^
type Pow struct{}

// Funcao da exponenciacao
func (Pow) Operacao(a, b float64) string {
	fmt.Printf("POW TA CHEGANDO ATE AQUI")
	pow := math.Pow(a, b)
	resp := pow/1000
	fmt.Println(resp)
	respstr := strconv.FormatFloat(resp, 'E', -1, 64)

	respstr = strings.Replace(respstr, "E+00", "", 3)
	respstr = strings.Replace(respstr, "E+01", "", 3)
	respstr = strings.Replace(respstr, "E+02", "", 3)
	respstr = strings.Replace(respstr, "E+03", "", 3)
	respstr = strings.Replace(respstr, "E+04", "", 3) // Pode chorar, eu chorei tambem dpos de fazer essa bosta
	respstr = strings.Replace(respstr, "E+05", "", 3)
	respstr = strings.Replace(respstr, "E+06", "", 3)
	respstr = strings.Replace(respstr, "E+07", "", 3)
	respstr = strings.Replace(respstr, "E+08", "", 3)
	respstr = strings.Replace(respstr, "E+09", "", 3)

	resposta := (respstr + "×10^3")
	fmt.Println("Resposta:", resposta)

	return resposta
}

// Struct para a operacao (ROT) raiz
type Rot struct{}

// Fucnao com a operacao de raiz
func (Rot) Operacao(a, b float64) string {
	rot := math.Pow(a, (1 / b))
	resp := rot/1000
	fmt.Println(resp)
	respstr := strconv.FormatFloat(resp, 'E', -1, 64)

	respstr = strings.Replace(respstr, "E+00", "", 3)
	respstr = strings.Replace(respstr, "E+01", "", 3)
	respstr = strings.Replace(respstr, "E+02", "", 3)
	respstr = strings.Replace(respstr, "E+03", "", 3)
	respstr = strings.Replace(respstr, "E+04", "", 3) // Pode chorar, eu chorei tambem dpos de fazer essa bosta
	respstr = strings.Replace(respstr, "E+05", "", 3)
	respstr = strings.Replace(respstr, "E+06", "", 3)
	respstr = strings.Replace(respstr, "E+07", "", 3)
	respstr = strings.Replace(respstr, "E+08", "", 3)
	respstr = strings.Replace(respstr, "E+09", "", 3)

	resposta := (respstr + "×10^3")
	fmt.Println("Resposta:", resposta)

	return resposta
}

// Mapa ligando as operacaoes especifcas e suas devidas funcoes
var OperacoesMapa = map[string]Operacoes{
	"add": Add{},
	"sub": Sub{},
	"mul": Mul{},
	"div": Div{},
	"pow": Pow{},
	"rot": Rot{},
}

// Funcao que executa a operacao do mapa
func (c Calculator) ExecutarOperacao(res http.ResponseWriter, action string, num1, num2 int) (string, error) {
	operacao, ok := OperacoesMapa[action]
	if !ok { //Se !ok, ele retorna erro pq a operacao escolhida nao existe no codigo
		//  Usando funcao SendInvalidOperationResponse para enviar uma resposta de operacao invalida em JSON
		SendInvalidOperationResponse(res, "Operação inválida: "+action)
		return "", nil
	}
	// Retorna o resultado
	resultado := operacao.Operacao(float64(num1), float64(num2))
	// Utiliza a função SendSuccessResponse do modulo json_response para enviar uma resposta de sucesso em JSON
	//SendSuccessResponse(res, resultado, action)
	return resultado, nil
}
