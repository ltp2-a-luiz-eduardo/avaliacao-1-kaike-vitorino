package main

import (
	"encoding/json"
	"net/http"
)

func SendSuccessResponse(res http.ResponseWriter, resultado string, operacao string) {
	res.Header().Set("Content-Type", "application/json")
	response := map[string]interface{}{
		"code":   200,
		"result": resultado,
	}
	// Ve se a operacao existe
	_, ok := OperacoesMapa[operacao]
	if !ok {
		// Se a operacao nn exsite, nao faz nada nada
		return
	}
	// Se a operacao existe, adiciona ela ao response
	response["op"] = operacao

	json.NewEncoder(res).Encode(response)
}

// Funcao para enviar uma resposta de operacao invalida em JSON
func SendInvalidOperationResponse(res http.ResponseWriter, mensagem string) {
	res.WriteHeader(http.StatusBadRequest)
	response := map[string]interface{}{
		"code":   400,
		"result": "invalid expression",
		"op":     mensagem,
	}
	json.NewEncoder(res).Encode(response)
}

// Funcao para enviar uma resposta de metodo ou path invalido em JSON
func SendInvalidPathResponse(res http.ResponseWriter) {
	res.WriteHeader(http.StatusNotFound)
	response := map[string]interface{}{
		"code":  404,
		"error": "not found",
	}
	json.NewEncoder(res).Encode(response)
}

// Funcao para enviar uma resposta de erro generico em JSON
func SendErrorResponse(res http.ResponseWriter) {
	res.WriteHeader(http.StatusInternalServerError)
	response := map[string]interface{}{
		"code":  500,
		"error": "something went wrong",
	}
	json.NewEncoder(res).Encode(response)
}
